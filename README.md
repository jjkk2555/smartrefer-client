# smart-refer
## HIS_PROVIDER = [ universal,phosp,mbase,homc,hi,hosxpv4,hosxpv4pg,hosxpv3,himpro,jhcis,ubase,sakon,homcudon,hosxpv3arjaro,softcon,softconudon ]
## DB_CLIENT = [ mysql2,mysql,mssql,pg ]
#
# ตั้งค่า

config.api

- ติดต่อฐานจ้อมูล HIS

HIS_PROVIDER=hi

DB_HOST=localhost

DB_CLIENT=mysql2

DB_PORT=3306

DB_NAME=his

DB_USER=his

DB_PASSWORD=12345678

- รหัสสถานบริการ 

HIS_CODE=27967

- Api เซื่อมต่อ Server กลาง

SERV_API_URL=http://xxxxxxxxx:xxx
SERV_COC_API_URL=http://xxxxxxxxx:xxx

# Setup

docker network create kong-network

docker-compose up -d

# Open

http://localhost:8080


# Update

docker-compose pull

docker-compose down

docker-compose up -d
